/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.javauser.registration.token;

import java.time.LocalDateTime;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 *
 * @author rohini
 */
@Service
@AllArgsConstructor
public class ConfirmationTokenService {
    private final ConfirmationTokenRepository conRepo;
    
    public void saveConfirmationToken(ConfirmationToken token){
        conRepo.save(token);
    }
    
      public Optional<ConfirmationToken> getToken(String token) {
        return conRepo.findByToken(token);
    }

    public int setConfirmedAt(String token) {
        return conRepo.updateConfirmedAt(
                token, LocalDateTime.now());
    }
}
