/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.javauser.appuser;

import com.example.javauser.registration.token.ConfirmationToken;
import com.example.javauser.registration.token.ConfirmationTokenService;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author rohini
 */
@Service
@AllArgsConstructor
public class AppUserService implements UserDetailsService {
    
    private final AppUserRepository appUserRepo;
    private final static String USER_NOT_FOUND_MSG = "user with email %s not found";
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ConfirmationTokenService confirmationTokenService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return appUserRepo.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(String.format(USER_NOT_FOUND_MSG,email)));
    }
    
    public String signUpUser(AppUser user){
        
        boolean userExists = appUserRepo.findByEmail(user.getEmail()).isPresent();
        if(userExists){
            throw new IllegalStateException("Email already taken.");
        }
        
        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        user.setEnabled(true);
        appUserRepo.save(user);
        
        String token = UUID.randomUUID().toString();
        ConfirmationToken conftoken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                user
        );
        
        confirmationTokenService.saveConfirmationToken(conftoken);
         return token;
    }
    
      public int enableAppUser(String email) {
        return appUserRepo.enableAppUser(email);
    }
    
}
