package com.example.javauser.employee;

import com.example.javauser.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DesignationService {
    private final DesignationRepository desRepo;

    @Autowired
    public DesignationService(DesignationRepository desRepo) {
        this.desRepo = desRepo;
    }

    public Designation addDesignation(Designation designation) {

        return desRepo.save(designation);
    }

    public List<Designation> findAllDesignation() {
        return desRepo.findAll();
    }

    public Designation updateDesignation(Designation designation) {
        return desRepo.save(designation);
    }

    public Designation findDesignationById(Long id) {
        return desRepo.findById(id).orElseThrow(
                () -> new UserNotFoundException("Designation by id" + id + " not found")
        );
    }

    public void deleteDesignation(Long id) {
        desRepo.deleteById(id);
    }


}
