package com.example.javauser.employee;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Designation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable=false,updatable=false)
    private Long id;

    private String title;

    public Designation() {
    }

    public Designation(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public String toString() {
        return "Designation{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
