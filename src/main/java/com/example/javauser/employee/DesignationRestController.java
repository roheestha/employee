package com.example.javauser.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/designation")
public class DesignationRestController {
    private final DesignationService designationService;

    public DesignationRestController(DesignationService designationService) {
        this.designationService = designationService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Designation>> getAllDesignations(){
        List<Designation> designations = designationService.findAllDesignation();
        return new ResponseEntity<>(designations, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Designation> addDesignation(@RequestBody Designation designation){
        Designation newDesignation = designationService.addDesignation(designation);
        return new ResponseEntity<>(newDesignation,HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<Designation> updateDesignation(@RequestBody Designation designation){
        Designation updateDesignation = designationService.updateDesignation(designation);
        return new ResponseEntity<>(updateDesignation,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteDesignation(@PathVariable("id") Long id){
        designationService.deleteDesignation(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
